package rea.codingexercise.toyrobot.utils;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.Test;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;
import rea.codingexercise.toyrobot.exception.RobotException;
import rea.codingexercise.toyrobot.model.Automaton;
import rea.codingexercise.toyrobot.model.RobotTest;
import rea.codingexercise.toyrobot.model.environment.Turn;

import java.util.Optional;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static rea.codingexercise.toyrobot.utils.RobotProtocolUtils.*;

@RunWith(Theories.class)
public class RobotProtocolUtilsTest extends RobotTest {

    private static final String PLACE = "PLACE";
    Mockery context = new Mockery();

    @DataPoints
    public static String[][] validPlaceCommand = {{PLACE, "0,0,NORTH"}, {PLACE, "0,0,EAST"},
            {PLACE, "0,0,SOUTH"}, {PLACE, "0,0,WEST"}};

    @Test
    public void testInvalidCommand() throws Exception {
        String[] invalidCommand = {"some invalid command"};
        optionalEquals(UNKNOWN_COMMAND_MSG + invalidCommand[0], RobotProtocolUtils.translateToRobot(invalidCommand, rob));
    }

    @Theory
    public void testValidPlaceCommand(String[] validPlaceCommand) throws Exception {
        RobotProtocolUtils.translateToRobot(validPlaceCommand, rob);
        //ex: validPlaceCommand = {PLACE, "0,0,EAST"}
        String expectedOrientation = validPlaceCommand[1].split(",")[2];
        assertEquals(expectedOrientation, rob.getFacing().name());
    }

    @Test
    public void testInvalidPositionPlaceCommand() throws Exception {
        String[] invalidCommand = {"PLACE", "X,%,NORTH"};
        optionalEquals(INVALID_POSITION_VALUES_MSG + "X,%", RobotProtocolUtils.translateToRobot(invalidCommand, rob));
    }

    @Test
    public void testInvalidOrientationPlaceCommand() throws Exception {
        String[] invalidCommand = {"PLACE", "1,2,UP"};
        optionalEquals(UNKNOWN_ORIENTATION_MSG +"UP", RobotProtocolUtils.translateToRobot(invalidCommand, rob));
    }

    @Test
    public void testMissingOrientationPlaceCommand() throws Exception {
        String[] invalidCommand = {"PLACE", "3,4"};
        optionalEquals(INVALID_COMMAND_MSG + "[3, 4]" + EXAMPLE_PLACE_PARAMETERS_MSG,
                RobotProtocolUtils.translateToRobot(invalidCommand, rob));
    }

    @Test
    public void testTooManyPlaceValuesCommand() throws Exception {
        String[] invalidCommand = {"PLACE", "1,2,UP,DOWN"};
        optionalEquals(INVALID_COMMAND_MSG + "[1, 2, UP, DOWN]" + EXAMPLE_PLACE_PARAMETERS_MSG,
                RobotProtocolUtils.translateToRobot(invalidCommand, rob));
    }

    @Test
    public void testMoveCommand() throws Exception {
        placeRob();
        String[] validCommand = {"MOVE"};

        context.checking(new Expectations() {{
            oneOf(rob).move();
        }});

        assertThat(RobotProtocolUtils.translateToRobot(validCommand, rob), is(Optional.empty()));
    }

    @Test
    public void testTurnRightCommand() throws Exception {
        placeRob();
        String[] validCommand = {"RIGHT"};

        context.checking(new Expectations() {{
            oneOf(rob).turn(Turn.RIGHT);
        }});

        assertThat(RobotProtocolUtils.translateToRobot(validCommand, rob), is(Optional.empty()));
    }

    @Test
    public void testTurnLeftCommand() throws Exception {
        placeRob();
        String[] validCommand = {"LEFT"};

        context.checking(new Expectations() {{
            oneOf(rob).turn(Turn.LEFT);
        }});

        assertThat(RobotProtocolUtils.translateToRobot(validCommand, rob), is(Optional.empty()));
    }

    @Test
    public void testReportCommand() throws Exception {
        placeRob();
        String[] validCommand = {"REPORT"};

        context.checking(new Expectations() {{
            oneOf(rob).report(); will(returnValue("0,0 EAST"));
        }});

        optionalEquals("0,0 EAST", RobotProtocolUtils.translateToRobot(validCommand, rob));
    }

    private void placeRob() throws RobotException {
        rob = context.mock(Automaton.class);
    }

    private void optionalEquals(String s, Optional<String> optional) {
        assertTrue(s.equals(optional.get()));
    }
}