package rea.codingexercise.toyrobot;

import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by Bertrand on 22/10/2014.
 */

public class RobotE2ETest {

    String[] args = {};
    ByteArrayOutputStream record;

    @Before
    public void setUp() {
        record = new ByteArrayOutputStream();
        PrintStream stream = new PrintStream(record);
        System.setOut(stream);
    }

    @Test
    public void testMainDefault() {
        Main.main(args);
        assertThat(record.toString(), containsString("4,2 EAST"));
    }

    @Test
    public void testMainNoFall() {
        args = new String[]{"./src/main/resources/testRobotNoFall.txt"};
        Main.main(args);
        assertThat(record.toString(), containsString("5,5 NORTH"));
    }

}
