package rea.codingexercise.toyrobot.utils;

import rea.codingexercise.toyrobot.exception.RobotException;
import rea.codingexercise.toyrobot.model.Automaton;
import rea.codingexercise.toyrobot.model.environment.Orientation;
import rea.codingexercise.toyrobot.model.environment.Position;
import rea.codingexercise.toyrobot.model.environment.Turn;

import java.util.Arrays;
import java.util.Optional;

/**
 * This class makes it easier to handle a robot.
 * <p>
 * Created by Bertrand on 28/09/2014.
 */
public class RobotProtocolUtils {

    private RobotProtocolUtils() {
        super();
    }

    static final String INVALID_COMMAND_MSG = "Invalid place command, too many or not enough parameters: ";
    static final String EXAMPLE_PLACE_PARAMETERS_MSG = " three parameters needed, ex: X,Y,NORTH";
    static final String INVALID_POSITION_VALUES_MSG = "Invalid position values: ";
    static final String UNKNOWN_ORIENTATION_MSG = "Unknown orientation given for translation: ";
    static final String UNKNOWN_COMMAND_MSG = "Unknown command: ";

    /**
     * Translate string array of instructions into actual robot command calls, and makes the call.
     *
     * @param instruction the instruction to pass on to the robot
     * @param rob         the robot to receive the instructions
     */
    public static Optional<String> translateToRobot(String[] instruction, Automaton rob) {
        Optional<String> response = Optional.empty();
        try {
            if ("PLACE".equals(instruction[0])) {
                response = handlePlace(instruction[1], rob);
            } else {
                switch (instruction[0]) {
                    case "MOVE":
                        rob.move();
                        break;
                    case "RIGHT":
                        rob.turn(Turn.RIGHT);
                        break;
                    case "LEFT":
                        rob.turn(Turn.LEFT);
                        break;
                    case "REPORT":
                        response = Optional.of(rob.report());
                        break;
                    default:
                        response = Optional.of(UNKNOWN_COMMAND_MSG + instruction[0]);
                }
            }
        } catch (RobotException e) {
            response = Optional.of(e.getMessage());
        }
        return response;
    }

    //The place command is a bit special
    private static Optional<String> handlePlace(String s, Automaton rob) throws RobotException {
        String[] placeCommands = s.split(",");
        Optional<String> response = Optional.empty();
        if (placeCommands.length != 3) {
            response = Optional.of(INVALID_COMMAND_MSG + Arrays.toString(placeCommands)
                    + EXAMPLE_PLACE_PARAMETERS_MSG);
        } else {
            rob.placeOnTableTop(stringsToPosition(placeCommands), stringToOrientation(placeCommands[2]));
        }
        return response;
    }

    private static Position stringsToPosition(String[] placeCommands) throws RobotException {
        Position position;
        try {
            position = new Position(Integer.parseInt(placeCommands[0]), Integer.parseInt(placeCommands[1]));
        } catch (NumberFormatException e) {
            throw new RobotException(INVALID_POSITION_VALUES_MSG + placeCommands[0] + "," + placeCommands[1], e);
        }
        return position;
    }

    /**
     * Converts a string into an orientation.
     *
     * @param orientation the object to transfor in position.
     * @return the position
     * @throws RobotException
     */
    private static Orientation stringToOrientation(String orientation) throws RobotException {

        Orientation result;
        // figure out which direction it is
        if (orientation.equals(Orientation.NORTH.name())) {
            result = Orientation.NORTH;
        } else if (orientation.equals(Orientation.EAST.name())) {
            result = Orientation.EAST;
        } else if (orientation.equals(Orientation.SOUTH.name())) {
            result = Orientation.SOUTH;
        } else if (orientation.equals(Orientation.WEST.name())) {
            result = Orientation.WEST;
        } else {
            throw new RobotException(UNKNOWN_ORIENTATION_MSG + orientation);
        }
        // return the position value of the orientation
        return result;
    }
}
