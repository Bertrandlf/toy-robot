package rea.codingexercise.toyrobot.utils;

import rea.codingexercise.toyrobot.exception.RobotException;
import rea.codingexercise.toyrobot.model.environment.Orientation;
import rea.codingexercise.toyrobot.model.environment.Position;
import rea.codingexercise.toyrobot.model.environment.Turn;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Robot utils to help the robot turn and move.
 * <p>
 * Created by Bertrand on 27/09/2014.
 */
public class RobotActionsUtils {

    private static final Map<Orientation, Orientation> left;
    private static final Map<Orientation, Orientation> right;

    static {
        //what is on the left of a given orientation
        Map<Orientation, Orientation> leftMap = new HashMap<>();
        leftMap.put(Orientation.EAST, Orientation.NORTH);
        leftMap.put(Orientation.SOUTH, Orientation.EAST);
        leftMap.put(Orientation.WEST, Orientation.SOUTH);
        leftMap.put(Orientation.NORTH, Orientation.WEST);
        left = Collections.unmodifiableMap(leftMap);

        //what is on the right of a given orientation
        Map<Orientation, Orientation> rightMap = new HashMap<>();
        rightMap.put(Orientation.EAST, Orientation.SOUTH);
        rightMap.put(Orientation.SOUTH, Orientation.WEST);
        rightMap.put(Orientation.WEST, Orientation.NORTH);
        rightMap.put(Orientation.NORTH, Orientation.EAST);
        right = Collections.unmodifiableMap(rightMap);
    }

    //utils class not for instantiation
    private RobotActionsUtils() {
        super();
    }

    /**
     * Add a position value, in this case the given position forms a vector with this
     */
    public static Position moveOf(Position origin, Position move) {
        return new Position(origin.getX() + move.getX(), origin.getY() + move.getY());
    }

    /**
     * Give the orientation that is left or right of a given orientation.
     *
     * @param orientation the orientation we inquire about
     * @param turn        right or left
     * @throws RobotException
     */
    public static Orientation getOrientation(Orientation orientation, Turn turn) throws RobotException {
        switch (turn) {
            case LEFT:
                return left.get(orientation);
            case RIGHT:
                return right.get(orientation);
            default:
                throw new RobotException("Unknown turn provided for orientation lookup: " + turn);
        }
    }
}
