package rea.codingexercise.toyrobot.model.impl;

import rea.codingexercise.toyrobot.exception.RobotException;
import rea.codingexercise.toyrobot.model.Automaton;
import rea.codingexercise.toyrobot.model.environment.Orientation;
import rea.codingexercise.toyrobot.model.environment.Position;
import rea.codingexercise.toyrobot.model.environment.TableTop;
import rea.codingexercise.toyrobot.model.environment.Turn;
import rea.codingexercise.toyrobot.utils.RobotActionsUtils;

import java.util.logging.Logger;

/**
 * A toy robot that will not fall
 * <p>
 * Created by Bertrand on 26/09/2014.
 */
public class Robot implements Automaton {

    private static final Logger LOG = Logger.getLogger(Robot.class.getName());

    //table top the robot is on
    private TableTop tableTop;

    //current position of the robot on the table top
    private Position position;

    //orientation of the robot
    private Orientation facing;

    /**
     * Advance one step in the direction I face
     */
    public void move() throws RobotException {
        if (isPlaced()) {
            LOG.fine("Moving of " + facing.getValue());
            Position newPotentialPosition = RobotActionsUtils.moveOf(position, facing.getValue());
            if (positionIsOnTheTable(newPotentialPosition)) {
                this.setPosition(newPotentialPosition);
            } else {
                LOG.fine("I was asked to jump of the table (" + facing + "), I'll just stay here :" + getPosition());
            }
        }
    }

    /**
     * @return returns the position and orientation of the robot
     * @throws RobotException
     */
    public String report() throws RobotException {
        if (isPlaced()) {
            return getPosition().toString() + " " + getFacing();
        } else {
            return "I am not placed";
        }
    }

    /**
     * @param position    where on the table should we place me
     * @param orientation where should I be facing
     * @throws RobotException
     */
    public void placeOnTableTop(Position position, Orientation orientation) throws RobotException {
        if (hasATable() && positionIsOnTheTable(position)) {
            setPosition(position);
            setFacing(orientation);
        } else {
            throw new RobotException("This position is not on a table, please do not put me there :" + position);
        }
    }

    /**
     * @return returns the current position of the robot
     */
    public Position getPosition() {
        return position;
    }

    /**
     * @param position position to set the robot
     */
    public void setPosition(Position position) {
        this.position = position;
    }

    /**
     * @return returns the orientation of the robot
     */
    public Orientation getFacing() {
        return facing;
    }

    /**
     * @param facing the orientation of the robot
     */
    public void setFacing(Orientation facing) {
        this.facing = facing;
    }

    /**
     * @return tableTop the table top under the robot
     */
    public TableTop getTableTop() {
        return tableTop;
    }

    /**
     * @param tableTop a tableTop to place the robot on
     */
    public void setTableTop(TableTop tableTop) {
        this.tableTop = tableTop;
    }

    /**
     * Rotate 90 degrees to the given turn
     */
    public void turn(Turn turn) throws RobotException {
        if (isPlaced()) {
            if (facing != null) {
                this.setFacing(RobotActionsUtils.getOrientation(facing, turn));
            }
        }
    }

    //check if I was assigned a table and a direction
    private boolean isPlaced() throws RobotException {
        return hasATable() && isFacingADirection();
    }

    //check if I was assigned a table
    private boolean hasATable() throws RobotException {
        if (tableTop == null) {
            throw new RobotException("Please assign me a table top first");
        } else {
            return true;
        }
    }

    //check if I was assigned a direction
    private boolean isFacingADirection() throws RobotException {
        if (facing == null) {
            throw new RobotException("Please assign me a direction first");
        } else {
            return true;
        }
    }

    //check if the position is safe
    private boolean positionIsOnTheTable(Position position) throws RobotException {
        return hasATable()
                && withinBoundaries(position.getX(), getTableTop().getWidth())
                && withinBoundaries(position.getY(), getTableTop().getHeight());
    }

    //check if the given value is within table boundary (one coordinate)
    private boolean withinBoundaries(int givenValue, int maxValue) {
        return givenValue >= 0 && givenValue <= maxValue;
    }
}
