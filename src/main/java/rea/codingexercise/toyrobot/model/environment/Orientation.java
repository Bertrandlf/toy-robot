package rea.codingexercise.toyrobot.model.environment;

/**
 * Representation of the possible orientations.
 *
 * Created by Bertrand on 26/09/2014.
 */
public enum Orientation {

    /** NORTH. */
    NORTH(0,1),

    /** EAST. */
    EAST(1,0),

    /** SOUTH. */
    SOUTH(0,-1),

    /** WEST. */
    WEST(-1,0);

    /** The orientation value, it is also the value of the move in this orientation. */
    private Position value;

    /**
     * Constructor.
     *
     * @param x horizontal value
     * @param y vertical value
     */
    Orientation(int x, int y){
        value  = new Position(x, y);
    }

    /**
     * Gets the orientation.
     *
     * @return Returns the value of the orientation.
     */
    public Position getValue() {
        return value;
    }

}
